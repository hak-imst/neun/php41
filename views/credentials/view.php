<div class="container">
    <h2>Zugangsdaten anzeigen</h2>

    <p>
        <a class="btn btn-primary" href="index.php?r=credentials/update&id=<?= $c->getId() ?>">Aktualisieren</a>
        <a class="btn btn-danger" href="index.php?r=credentials/delete&id=<?= $c->getId() ?>">Löschen</a>
        <a class="btn btn-default" href="index.php?r=credentials/index">Zurück</a>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Name</th>
            <td><?= $c->getName() ?></td>
        </tr>
        <tr>
            <th>Domäne</th>
            <td><?= $c->getDomain() ?></td>
        </tr>
        <tr>
            <th>CMS-Benutzername</th>
            <td><?= $c->getCmsUsername() ?></td>
        </tr>
        <tr>
            <th>CMS-Passwort</th>
            <td><?= $c->getCmsPassword() ?></td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->
