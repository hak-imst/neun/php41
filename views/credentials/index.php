<div class="container">
    <div class="row">
        <h2>Passwortmanager</h2>
    </div>
    <div class="row">
        <p>
            <a href="index.php?r=credentials/create" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>
        <p>
            <input id="search" type="text" class="form-control" >
            <input type="button" value="Suchen" class="btn btn-info" onclick="search()">
            <input type="reset" value="Reset" class="btn btn-info" onclick="clear()">
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Domäne</th>
                <th>CMS-Benutzername</th>
                <th>CMS-Passwort</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="table">
            <?php

            foreach ($credentials as $c) {
                echo '<tr>';
                echo '<td>' . $c->getName() . '</td>';
                echo '<td>' . $c->getDomain() . '</td>';
                echo '<td>' . $c->getCmsUsername() . '</td>';
                echo '<td>' . $c->getCmsPassword() . '</td>';
                echo '<td>';
                echo '<a class="btn btn-info" href="index.php?r=credentials/view&id=' . $c->getId() . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                echo '&nbsp;';
                echo '<a class="btn btn-primary" href="index.php?r=credentials/update&id=' . $c->getId() . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                echo '&nbsp;';
                echo '<a class="btn btn-danger" href="index.php?r=credentials/delete&id=' . $c->getId() . '"><span class="glyphicon glyphicon-remove"></span></a>';
                echo '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div> <!-- /container -->
