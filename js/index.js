
function clear() {
    alert("todo");
}

function search() {

    var search = document.getElementById('search').value;

    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            var html = '';

            JSON.parse(this.responseText).forEach(function (item, index) {

                html += '<tr>' +
                    '<td>' + item.name + '</td>' +
                    '<td>' + item.domain + '</td>' +
                    '<td>' + item.cms_username + '</td>' +
                    '<td>' + item.cms_password + '</td>' +
                    '<td><a class="btn btn-info" ' +
                    'href="index.php?r=credentials/view.php?id=' + item.id + '">' +
                    '<span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;' +
                    '<a class="btn btn-primary" ' +
                    'href="index.php?r=credentials/update&amp;id=' + item.id + '">' +
                    '<span class="glyphicon glyphicon-pencil"></span></a>&nbsp;' +
                    '<a class="btn btn-danger" ' +
                    'href="index.php?r=credentials/delete&amp;id=' + item.id + '">' +
                    '<span class="glyphicon glyphicon-remove"></span></a></td>' +
                    '</tr>';
            });

            document.getElementById('table').innerHTML = html;

        }
    };

    xhttp.open("GET", "php41/api/credentials/search/" + search);
    xhttp.send();
}
