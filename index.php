<?php

require_once 'controllers/CredentialsController.php';


if (!isset($_GET['r'])) {
    header("Location: index.php?r=credentials/index");
    exit();
}

$route = $_GET['r'];

$tokens = explode('/', $route);

if ($tokens[0] == 'credentials') {

    // credentials/index
    // credentials/view
    // credentials/delete
    // credentials/update
    $controller = new CredentialsController();
    $controller->action($tokens[1]);
} else {
    echo "Fehler";
}



