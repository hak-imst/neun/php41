<?php

require_once 'models/Credentials.php';

class CredentialsController
{
    public function action($action) {
            switch ($action) {
                case 'index':
                    $this->index();
                    break;
                case 'create':
                    $this->create();
                    break;
                case 'delete':
                    $this->delete();
                    break;
                case 'update':
                    $this->update();
                    break;
                case 'view':
                    $this->view();
                    break;
                default:
                    echo "Unknown action: $action";
                    break;
            }
    }

    private function index()
    {
        // load and show all credentials from database
        $credentials = Credentials::getAll();
        $title = 'Alle Credentials anzeigen';

        include 'views/layout/top.php';
        include 'views/credentials/index.php';
        include 'views/layout/bottom.php';
    }

    private function create()
    {
        // create new/empty model, contains all input-values and error-messages
        $c = new Credentials();

        if (!empty($_POST)) {

            $c->setName(isset($_POST['name']) ? $_POST['name'] : '');
            $c->setDomain(isset($_POST['domain']) ? $_POST['domain'] : '');
            $c->setCmsUsername(isset($_POST['cms_username']) ? $_POST['cms_username'] : '');
            $c->setCmsPassword(isset($_POST['cms_password']) ? $_POST['cms_password'] : '');

            // redirect to view-page after successfull saving
            if ($c->save()) {
                header("Location: index.php?r=credentials/view&id=" . $c->getId());
                exit();
            }
        }

        $title = 'Credentials erstellen';
        include 'views/layout/top.php';
        include 'views/credentials/create.php';
        include 'views/layout/bottom.php';
    }

    private function view()
    {
        // view single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
        if (empty($_GET['id'])) {
            header("Location: index.php?credentials/index");
            exit();
        } else if (!is_numeric($_GET['id'])) {
            http_response_code(400);
            die();
        } else {
            // load single item per ID
            $c = Credentials::get($_GET['id']);
        }

        // check if item could be found
        if ($c == null) {
            http_response_code(404);    // item not found
            die();
        }

        $title = 'Credentials anzeigen';
        include 'views/layout/top.php';
        include 'views/credentials/view.php';
        include 'views/layout/bottom.php';
    }

    private function update()
    {
        // load single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
        if (empty($_GET['id'])) {
            header("Location: index.php?credentials/index");
            exit();
        } else if (!is_numeric($_GET['id'])) {
            http_response_code(400);
            die();
        } else {
            // load single item per ID
            $c = Credentials::get($_GET['id']);
        }

        // check if item could be found
        if ($c == null) {
            http_response_code(404);    // item not found
            die();
        }

        if (!empty($_POST)) {
            $c->setName(isset($_POST['name']) ? $_POST['name'] : '');
            $c->setDomain(isset($_POST['domain']) ? $_POST['domain'] : '');
            $c->setCmsUsername(isset($_POST['cms_username']) ? $_POST['cms_username'] : '');
            $c->setCmsPassword(isset($_POST['cms_password']) ? $_POST['cms_password'] : '');

            // update existing item and redirec to index-page
            if ($c->save()) {
                header("Location: index.php?r=credentials/view&id=" . $c->getId());
                exit();
            }
        }

        $title = 'Credentials bearbeiten';
        include 'views/layout/top.php';
        include 'views/credentials/update.php';
        include 'views/layout/bottom.php';
    }

    private function delete()
    {
        // get ID of item (HTTP GET) for showing
        $id = !empty($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : 0;

        if (!empty($_POST['id'])) {

            if (!is_numeric($_POST['id'])) {
                http_response_code(400);
                die();
            }

            Credentials::delete($_POST['id']);

            header("Location: index.php?r=credentials/index");
            exit();

        } else {
            $c = Credentials::get($id); // load item for showing

            // check if item could be found
            if ($c == null) {
                http_response_code(404);    // item not found
                die();
            }
        }

        $title = 'Credentials löschen';
        include 'views/layout/top.php';
        include 'views/credentials/delete.php';
        include 'views/layout/bottom.php';
    }


}
