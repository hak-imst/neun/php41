# Passwortmanager - Musterlösung - php31

Dies ist die Musterlösung zur Aufgabe "Passwortmanager". 
Darin werden die Grundlagen des Datenbankzugriffs und Object-Relational-Mapping (ORM) behandlet.

Aufruf der GUI:
http://localhost/php41/index.php?r=credentials/index

Aufruf der API:
http://localhost/php41/api/credentials
